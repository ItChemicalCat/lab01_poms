﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityNative.Toasts.Example;

public class Main : MonoBehaviour
{
    [SerializeField] private InputField inputName;
    [SerializeField] private InputField inputSurname;
    [SerializeField] private Button buttonResult;
    [SerializeField] private Text textResult;

    private void Awake() //сразу после создания объекта
    {
        if (textResult != null)
        {
            textResult.text = "";
        }
    }
    
    private void Start() //на следующий кадр после создания объекта
    {
        if (buttonResult != null)
        {
            buttonResult.onClick.AddListener(OnClicked);
        }
    }

    private void OnDestroy() //при уничтожении объекта
    {
        if (buttonResult != null)
        {
            buttonResult.onClick.RemoveListener(OnClicked);
        }
    }

    private void ShowToast(string message)
    {
#if UNITY_ANDROID
        UnityNativeToastsHelper.ShowShortText(message);
#endif
    }

    private void SetTextResult(string result)
    {
        if (textResult != null)
        {
            textResult.text = result;
        }
    }
    
    public void OnClicked()
    {
        SetTextResult("");
        
        if (inputName == null || inputSurname == null) 
            return;
        
        if (inputName.text == "")
        {
            ShowToast("Name is empty");
            return;
        }
        
        if (inputSurname.text == "")
        {
            ShowToast("Surname is empty");
            return;
        }
        
        var result = $"Welcome {inputName.text} {inputSurname.text}";
        SetTextResult(result);

        ShowToast(result);
    }
}
